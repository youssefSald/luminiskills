package com.luminiskills.repository;

import java.util.Optional;

import com.luminiskills.domain.Candidate;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data repository for the Candidate entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CandidateRepository extends JpaRepository<Candidate, Long> {

    @Query(value = "SELECT * FROM candidate WHERE user_id = :userId", nativeQuery = true)
    Optional<Candidate> findCurrentById(@Param("userId") Long userId);

}
