package com.luminiskills.repository;

import java.util.List;

import com.luminiskills.domain.Application;
import com.luminiskills.domain.Offer;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data repository for the Offer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OfferRepository extends JpaRepository<Offer, Long> {

    @Query(value = "SELECT * FROM offer o WHERE LOWER(description) like %:search% or LOWER(title) like %:search%", nativeQuery = true)
    List<Offer> search(@Param("search") String search);

}
