package com.luminiskills.repository;

import java.util.List;

import com.luminiskills.domain.Application;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data repository for the Application entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long> {

    @Query(value = "SELECT * FROM application WHERE offer_id = :offer", nativeQuery = true)
    List<Application> findApplications(@Param("offer") Long offer);

    @Query(value = "SELECT * FROM application a WHERE candidate_id = :candidate and  offer_id = :offer", nativeQuery = true)
    Application applied(@Param("candidate") Long candidate, @Param("offer") Long offer);

    @Query(value = "SELECT * FROM application a WHERE offer_id = :offerId", nativeQuery = true)
    List<Application> usersApplied(@Param("offerId") Long offerId);
}
