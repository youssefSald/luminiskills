package com.luminiskills.web.rest;

import java.util.*;

import com.luminiskills.domain.Application;
import com.luminiskills.domain.Offer;
import com.luminiskills.service.ApplicationService;
import com.luminiskills.service.OfferService;
import com.luminiskills.service.dto.SearchDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.github.jhipster.web.util.ResponseUtil;

@RestController
@RequestMapping("/users")
public class InterclassResource {

    private final Logger log = LoggerFactory.getLogger(UserResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    @Autowired
    OfferService offerService;

    @Autowired
    ApplicationService applicationService;

    public InterclassResource() {
    }

    @GetMapping("/applications/{id}")
    public ResponseEntity<Application> getApplication(@PathVariable Long id) {
        log.debug("REST request to get Application : {}", id);
        Optional<Application> application = applicationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(application);
    }

    @GetMapping("/offers")
    public List<Offer> getAllOffers() {
        log.debug("REST request to get all Offers");
        return offerService.findAll();
    }

    @PostMapping("/offers/search")
    public List<Offer> search(@RequestBody SearchDTO search) {
        log.debug("REST request to get all Offers");
        return offerService.search(search.getSearch());
    }

    @GetMapping("/offers/{id}")
    public ResponseEntity<Offer> getOffer(@PathVariable Long id) {
        log.debug("REST request to get Offer : {}", id);
        Optional<Offer> offer = offerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(offer);
    }

}
