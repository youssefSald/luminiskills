package com.luminiskills.web.rest;

import com.luminiskills.domain.Recruiter;
import com.luminiskills.service.RecruiterService;
import com.luminiskills.service.dto.RecruiterDTO;
import com.luminiskills.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.luminiskills.domain.Recruiter}.
 */
@RestController
@RequestMapping("/api")
public class RecruiterResource {

    private final Logger log = LoggerFactory.getLogger(RecruiterResource.class);

    private static final String ENTITY_NAME = "recruiter";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RecruiterService recruiterService;

    public RecruiterResource(RecruiterService recruiterService) {
        this.recruiterService = recruiterService;
    }

    /**
     * {@code POST  /recruiters} : Create a new recruiter.
     *
     * @param recruiter the recruiter to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new recruiter, or with status {@code 400 (Bad Request)} if the recruiter has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/recruiters")
    public ResponseEntity<Recruiter> createRecruiter(@RequestBody RecruiterDTO recruiter) throws URISyntaxException {
        log.debug("REST request to save Recruiter : {}", recruiter);
        if (recruiter.getId() != null) {
            throw new BadRequestAlertException("A new recruiter cannot already have an ID", ENTITY_NAME, "idexists");
        }
        
        return recruiterService.save(recruiter);
    }

    /**
     * {@code PUT  /recruiters} : Updates an existing recruiter.
     *
     * @param recruiter the recruiter to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated recruiter,
     * or with status {@code 400 (Bad Request)} if the recruiter is not valid,
     * or with status {@code 500 (Internal Server Error)} if the recruiter couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/recruiters")
    public ResponseEntity<Recruiter> updateRecruiter(@RequestBody RecruiterDTO recruiter) throws URISyntaxException {
        log.debug("REST request to update Recruiter : {}", recruiter);
        if (recruiter.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        
        return recruiterService.update(recruiter);
    }

    /**
     * {@code GET  /recruiters} : get all the recruiters.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of recruiters in body.
     */
    @GetMapping("/recruiters")
    public List<Recruiter> getAllRecruiters() {
        log.debug("REST request to get all Recruiters");
        return recruiterService.findAll();
    }

    /**
     * {@code GET  /recruiters/:id} : get the "id" recruiter.
     *
     * @param id the id of the recruiter to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the recruiter, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/recruiters/{id}")
    public ResponseEntity<Recruiter> getRecruiter(@PathVariable Long id) {
        log.debug("REST request to get Recruiter : {}", id);
        Optional<Recruiter> recruiter = recruiterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(recruiter);
    }

    /**
     * {@code DELETE  /recruiters/:id} : delete the "id" recruiter.
     *
     * @param id the id of the recruiter to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/recruiters/{id}")
    public ResponseEntity<Void> deleteRecruiter(@PathVariable Long id) {
        log.debug("REST request to delete Recruiter : {}", id);
        recruiterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
