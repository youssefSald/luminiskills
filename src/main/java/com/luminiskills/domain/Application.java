package com.luminiskills.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Application.
 */
@Entity
@Table(name = "application")
public class Application implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "creation_date")
    private LocalDate creationDate;

    @Column(name = "new_application")
    private Boolean newApplication;

    @Column(name = "state")
    private String state;

    @Column(name = "comment")
    private String comment;

    @OneToOne
    @JoinColumn(unique = true)
    private Step step;

    @OneToOne
    private Offer offer;

    @ManyToOne
    @JsonIgnore
    private Candidate candidate;

    public Application() {

    }

    public Application(Long id, LocalDate creationDate, Boolean newApplication, String state, String comment, Step step,
            Offer offer, Candidate candidate) {
        this.id = id;
        this.creationDate = creationDate;
        this.newApplication = newApplication;
        this.state = state;
        this.comment = comment;
        this.step = step;
        this.offer = offer;
        this.candidate = candidate;
    }

    public Application(LocalDate creationDate, Boolean newApplication, String state, String comment, Step step,
            Offer offer, Candidate candidate) {
        this.creationDate = creationDate;
        this.newApplication = newApplication;
        this.state = state;
        this.comment = comment;
        this.step = step;
        this.offer = offer;
        this.candidate = candidate;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public Application creationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public Boolean isNewApplication() {
        return newApplication;
    }

    public Application newApplication(Boolean newApplication) {
        this.newApplication = newApplication;
        return this;
    }

    public void setNewApplication(Boolean newApplication) {
        this.newApplication = newApplication;
    }

    public String getState() {
        return state;
    }

    public Application state(String state) {
        this.state = state;
        return this;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getComment() {
        return comment;
    }

    public Application comment(String comment) {
        this.comment = comment;
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Step getStep() {
        return step;
    }

    public void setStep(Step step) {
        this.step = step;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public Application candidate(Candidate candidate) {
        this.candidate = candidate;
        return this;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((candidate == null) ? 0 : candidate.hashCode());
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((newApplication == null) ? 0 : newApplication.hashCode());
        result = prime * result + ((offer == null) ? 0 : offer.hashCode());
        result = prime * result + ((state == null) ? 0 : state.hashCode());
        result = prime * result + ((step == null) ? 0 : step.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Application other = (Application) obj;
        if (candidate == null) {
            if (other.candidate != null)
                return false;
        } else if (!candidate.equals(other.candidate))
            return false;
        if (comment == null) {
            if (other.comment != null)
                return false;
        } else if (!comment.equals(other.comment))
            return false;
        if (creationDate == null) {
            if (other.creationDate != null)
                return false;
        } else if (!creationDate.equals(other.creationDate))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (newApplication == null) {
            if (other.newApplication != null)
                return false;
        } else if (!newApplication.equals(other.newApplication))
            return false;
        if (offer == null) {
            if (other.offer != null)
                return false;
        } else if (!offer.equals(other.offer))
            return false;
        if (state == null) {
            if (other.state != null)
                return false;
        } else if (!state.equals(other.state))
            return false;
        if (step == null) {
            if (other.step != null)
                return false;
        } else if (!step.equals(other.step))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Application [candidate=" + candidate + ", comment=" + comment + ", creationDate=" + creationDate
                + ", id=" + id + ", newApplication=" + newApplication + ", offer=" + offer + ", state=" + state
                + ", stepId=" + step + "]";
    }

}
