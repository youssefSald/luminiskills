package com.luminiskills.domain;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Task entity.\n@author The JHipster team.
 */
@ApiModel(description = "Task entity.\n@author The JHipster team.")
@Entity
@Table(name = "company")
public class Company implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "logo")
    private String logo;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "postal_code")
    private Integer postalCode;

    @Column(name = "city")
    private String city;

    @Column(name = "country")
    private String country;

    @OneToMany(mappedBy = "company")
    private Set<Recruiter> recruiters = new HashSet<>();

    public Company() {
    }

    public Company(Long id, String logo, String name, String address, Integer postalCode, String city, String country,
            Set<Recruiter> recruiters) {
        this.id = id;
        this.logo = logo;
        this.name = name;
        this.address = address;
        this.postalCode = postalCode;
        this.city = city;
        this.country = country;
        this.recruiters = recruiters;
    }

    public Company(Long id, String logo, String name, String address, Integer postalCode, String city, String country) {
        this.id = id;
        this.logo = logo;
        this.name = name;
        this.address = address;
        this.postalCode = postalCode;
        this.city = city;
        this.country = country;
    }

    public Company(String logo, String name, String address, Integer postalCode, String city, String country,
            Set<Recruiter> recruiters) {
        this.logo = logo;
        this.name = name;
        this.address = address;
        this.postalCode = postalCode;
        this.city = city;
        this.country = country;
        this.recruiters = recruiters;
    }

    public Company(String logo, String name, String address, Integer postalCode, String city, String country) {
        this.logo = logo;
        this.name = name;
        this.address = address;
        this.postalCode = postalCode;
        this.city = city;
        this.country = country;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }

    public Company logo(String logo) {
        this.logo = logo;
        return this;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public Company name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public Company address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPostalCode() {
        return postalCode;
    }

    public Company postalCode(Integer postalCode) {
        this.postalCode = postalCode;
        return this;
    }

    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public Company city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public Company country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Set<Recruiter> getRecruiters() {
        return recruiters;
    }

    public Company recruiters(Set<Recruiter> recruiters) {
        this.recruiters = recruiters;
        return this;
    }

    public Company addRecruiter(Recruiter recruiter) {
        this.recruiters.add(recruiter);
        recruiter.setCompany(this);
        return this;
    }

    public Company removeRecruiter(Recruiter recruiter) {
        this.recruiters.remove(recruiter);
        recruiter.setCompany(null);
        return this;
    }

    public void setRecruiters(Set<Recruiter> recruiters) {
        this.recruiters = recruiters;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Company)) {
            return false;
        }
        return id != null && id.equals(((Company) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Company{" + "id=" + getId() + ", logo='" + getLogo() + "'" + ", name='" + getName() + "'"
                + ", address='" + getAddress() + "'" + ", postalCode=" + getPostalCode() + ", city='" + getCity() + "'"
                + ", country='" + getCountry() + "'" + "}";
    }
}
