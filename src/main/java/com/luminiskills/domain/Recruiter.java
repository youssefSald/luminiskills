package com.luminiskills.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Recruiter.
 */
@Entity
@Table(name = "recruiter")
public class Recruiter implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "post")
    private String post;

    @OneToMany(mappedBy = "recruiter")
    private Set<Offer> offers = new HashSet<>();

    @ManyToOne
    private Company company;

    public Recruiter() {
    }

    public Recruiter(Long id, String post, Set<Offer> offers, Company company) {
        this.id = id;
        this.post = post;
        this.offers = offers;
        this.company = company;
    }

    public Recruiter(String post, Set<Offer> offers, Company company) {
        this.post = post;
        this.offers = offers;
        this.company = company;
    }

    public Recruiter(String post, Company company) {
        this.post = post;
        this.company = company;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPost() {
        return post;
    }

    public Recruiter post(String post) {
        this.post = post;
        return this;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public Set<Offer> getOffers() {
        return offers;
    }

    public Recruiter offers(Set<Offer> offers) {
        this.offers = offers;
        return this;
    }

    public Recruiter addOffer(Offer offer) {
        this.offers.add(offer);
        offer.setRecruiter(this);
        return this;
    }

    public Recruiter removeOffer(Offer offer) {
        this.offers.remove(offer);
        offer.setRecruiter(null);
        return this;
    }

    public void setOffers(Set<Offer> offers) {
        this.offers = offers;
    }

    public Company getCompany() {
        return company;
    }

    public Recruiter company(Company company) {
        this.company = company;
        return this;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Recruiter)) {
            return false;
        }
        return id != null && id.equals(((Recruiter) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Recruiter{" + "id=" + getId() + ", post='" + getPost() + "'" + "}";
    }
}
