package com.luminiskills.domain;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Candidate.
 */
@Entity
@Table(name = "candidate")
public class Candidate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "driverslicense")
    private String driverslicense;

    @Column(name = "mobility")
    private String mobility;

    @Column(name = "city")
    private String city;

    @Column(name = "address")
    private String address;

    @Column(name = "cv")
    private String cv;

    @Column(name = "bith_date")
    private LocalDate bithDate;

    @Column(name = "handicap_situation")
    private Boolean handicapSituation;

    @Column(name = "full_profil")
    private Boolean fullProfil;

    @Column(name = "without_experience")
    private Boolean withoutExperience;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "candidate")
    private Set<Application> applications = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "candidate")
    private Set<Experience> experiences = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "candidate")
    private Set<Training> trainings = new HashSet<>();

    public Candidate() {
    }

    /**
     * @param id
     * @param driverslicense
     * @param mobility
     * @param city
     * @param address
     * @param cv
     * @param bithDate
     * @param handicapSituation
     * @param fullProfil
     * @param withoutExperience
     * @param user
     */
    public Candidate(String driverslicense, String mobility, String city, String address, String cv, LocalDate bithDate,
            Boolean handicapSituation, Boolean fullProfil, Boolean withoutExperience, User user) {
        this.driverslicense = driverslicense;
        this.mobility = mobility;
        this.city = city;
        this.address = address;
        this.cv = cv;
        this.bithDate = bithDate;
        this.handicapSituation = handicapSituation;
        this.fullProfil = fullProfil;
        this.withoutExperience = withoutExperience;
        this.user = user;
    }

    public Candidate(String driverslicense, String mobility, String city, String address, String cv, LocalDate bithDate,
            Boolean handicapSituation, Boolean fullProfil, Boolean withoutExperience, Set<Application> applications,
            Set<Experience> experiences, Set<Training> trainings) {
        this.driverslicense = driverslicense;
        this.mobility = mobility;
        this.city = city;
        this.address = address;
        this.cv = cv;
        this.bithDate = bithDate;
        this.handicapSituation = handicapSituation;
        this.fullProfil = fullProfil;
        this.withoutExperience = withoutExperience;
        this.applications = applications;
        this.experiences = experiences;
        this.trainings = trainings;
    }

    /**
     * @param id
     * @param driverslicense
     * @param mobility
     * @param city
     * @param address
     * @param cv
     * @param bithDate
     * @param handicapSituation
     * @param fullProfil
     * @param withoutExperience
     * @param user
     * @param applications
     * @param experiences
     * @param trainings
     */
    public Candidate(Long id, String driverslicense, String mobility, String city, String address, String cv,
            LocalDate bithDate, Boolean handicapSituation, Boolean fullProfil, Boolean withoutExperience, User user,
            Set<Application> applications, Set<Experience> experiences, Set<Training> trainings) {
        this.id = id;
        this.driverslicense = driverslicense;
        this.mobility = mobility;
        this.city = city;
        this.address = address;
        this.cv = cv;
        this.bithDate = bithDate;
        this.handicapSituation = handicapSituation;
        this.fullProfil = fullProfil;
        this.withoutExperience = withoutExperience;
        this.user = user;
        this.applications = applications;
        this.experiences = experiences;
        this.trainings = trainings;
    }

    public Candidate(Long id, String driverslicense, String mobility, String city, String address, String cv,
            LocalDate bithDate, Boolean handicapSituation, Boolean fullProfil, Boolean withoutExperience, User user) {
        this.id = id;
        this.driverslicense = driverslicense;
        this.mobility = mobility;
        this.city = city;
        this.address = address;
        this.cv = cv;
        this.bithDate = bithDate;
        this.handicapSituation = handicapSituation;
        this.fullProfil = fullProfil;
        this.withoutExperience = withoutExperience;
        this.user = user;
    }

    public Candidate(String driverslicense, String mobility, String city, String address, String cv, LocalDate bithDate,
            Boolean handicapSituation, Boolean fullProfil, Boolean withoutExperience, User user,
            Set<Application> applications, Set<Experience> experiences, Set<Training> trainings) {
        this.driverslicense = driverslicense;
        this.mobility = mobility;
        this.city = city;
        this.address = address;
        this.cv = cv;
        this.bithDate = bithDate;
        this.handicapSituation = handicapSituation;
        this.fullProfil = fullProfil;
        this.withoutExperience = withoutExperience;
        this.user = user;
        this.applications = applications;
        this.experiences = experiences;
        this.trainings = trainings;
    }

    /**
     * @return the handicapSituation
     */
    public Boolean getHandicapSituation() {
        return handicapSituation;
    }

    /**
     * @return the fullProfil
     */
    public Boolean getFullProfil() {
        return fullProfil;
    }

    /**
     * @return the withoutExperience
     */
    public Boolean getWithoutExperience() {
        return withoutExperience;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDriverslicense() {
        return driverslicense;
    }

    public Candidate driverslicense(String driverslicense) {
        this.driverslicense = driverslicense;
        return this;
    }

    public void setDriverslicense(String driverslicense) {
        this.driverslicense = driverslicense;
    }

    public String getMobility() {
        return mobility;
    }

    public Candidate mobility(String mobility) {
        this.mobility = mobility;
        return this;
    }

    public void setMobility(String mobility) {
        this.mobility = mobility;
    }

    public String getCity() {
        return city;
    }

    public Candidate city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public Candidate address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCv() {
        return cv;
    }

    public Candidate cv(String cv) {
        this.cv = cv;
        return this;
    }

    public void setCv(String cv) {
        this.cv = cv;
    }

    public LocalDate getBithDate() {
        return bithDate;
    }

    public Candidate bithDate(LocalDate bithDate) {
        this.bithDate = bithDate;
        return this;
    }

    public void setBithDate(LocalDate bithDate) {
        this.bithDate = bithDate;
    }

    public Boolean isHandicapSituation() {
        return handicapSituation;
    }

    public Candidate handicapSituation(Boolean handicapSituation) {
        this.handicapSituation = handicapSituation;
        return this;
    }

    public void setHandicapSituation(Boolean handicapSituation) {
        this.handicapSituation = handicapSituation;
    }

    public Boolean isFullProfil() {
        return fullProfil;
    }

    public Candidate fullProfil(Boolean fullProfil) {
        this.fullProfil = fullProfil;
        return this;
    }

    public void setFullProfil(Boolean fullProfil) {
        this.fullProfil = fullProfil;
    }

    public Boolean isWithoutExperience() {
        return withoutExperience;
    }

    public Candidate withoutExperience(Boolean withoutExperience) {
        this.withoutExperience = withoutExperience;
        return this;
    }

    public void setWithoutExperience(Boolean withoutExperience) {
        this.withoutExperience = withoutExperience;
    }

    public Set<Application> getApplications() {
        return applications;
    }

    public Candidate applications(Set<Application> applications) {
        this.applications = applications;
        return this;
    }

    public Candidate addApplication(Application application) {
        this.applications.add(application);
        application.setCandidate(this);
        return this;
    }

    public Candidate removeApplication(Application application) {
        this.applications.remove(application);
        application.setCandidate(null);
        return this;
    }

    public void setApplications(Set<Application> applications) {
        this.applications = applications;
    }

    public Set<Experience> getExperiences() {
        return experiences;
    }

    public Candidate experiences(Set<Experience> experiences) {
        this.experiences = experiences;
        return this;
    }

    public Candidate addExperience(Experience experience) {
        this.experiences.add(experience);
        experience.setCandidate(this);
        return this;
    }

    public Candidate removeExperience(Experience experience) {
        this.experiences.remove(experience);
        experience.setCandidate(null);
        return this;
    }

    public void setExperiences(Set<Experience> experiences) {
        this.experiences = experiences;
    }

    public Set<Training> getTrainings() {
        return trainings;
    }

    public Candidate trainings(Set<Training> trainings) {
        this.trainings = trainings;
        return this;
    }

    public Candidate addTraining(Training training) {
        this.trainings.add(training);
        training.setCandidate(this);
        return this;
    }

    public Candidate removeTraining(Training training) {
        this.trainings.remove(training);
        training.setCandidate(null);
        return this;
    }

    public void setTrainings(Set<Training> trainings) {
        this.trainings = trainings;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Candidate)) {
            return false;
        }
        return id != null && id.equals(((Candidate) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */

    @Override
    public String toString() {
        return "Candidate [address=" + address + ", applications=" + applications + ", bithDate=" + bithDate + ", city="
                + city + ", cv=" + cv + ", driverslicense=" + driverslicense + ", experiences=" + experiences
                + ", fullProfil=" + fullProfil + ", handicapSituation=" + handicapSituation + ", id=" + id
                + ", mobility=" + mobility + ", trainings=" + trainings + ", user=" + user + ", withoutExperience="
                + withoutExperience + "]";
    }

}
