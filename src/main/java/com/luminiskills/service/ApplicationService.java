package com.luminiskills.service;

import com.luminiskills.domain.Application;
import com.luminiskills.service.dto.ApplicationDTO;
import com.luminiskills.service.dto.ApplicationDataDTO;
import com.luminiskills.service.dto.AppliedDTO;

import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Application}.
 */
public interface ApplicationService {

    /**
     * Save a application.
     *
     * @param application the entity to save.
     * @return the persisted entity.
     */
    ResponseEntity<Application> save(ApplicationDTO application);

    ResponseEntity<Application> update(ApplicationDTO application);

    /**
     * Get all the applications.
     *
     * @return the list of entities.
     */
    List<Application> findAll();

    /**
     * Get the "id" application.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Application> findOne(Long id);

    Application applied(Long id);

    Application findApplication(ApplicationDataDTO id);

    /**
     * Delete the "id" application.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
