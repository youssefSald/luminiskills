package com.luminiskills.service;

import com.luminiskills.domain.Candidate;
import com.luminiskills.service.dto.CandidateDTO;

import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Candidate}.
 */
public interface CandidateService {

    /**
     * Save a candidate.
     *
     * @param candidate the entity to save.
     * @return the persisted entity.
     */
    ResponseEntity<Candidate> save(CandidateDTO candidate);

    ResponseEntity<Candidate> update(CandidateDTO candidate);

    /**
     * Get all the candidates.
     *
     * @return the list of entities.
     */
    List<Candidate> findAll();

    /**
     * Get the "id" candidate.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Candidate> findOne(Long id);

    Optional<Candidate> findcurrentOne(Long id);

    /**
     * Delete the "id" candidate.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
