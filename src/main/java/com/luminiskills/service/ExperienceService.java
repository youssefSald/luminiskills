package com.luminiskills.service;

import com.luminiskills.domain.Experience;
import com.luminiskills.service.dto.ExperienceDTO;

import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Experience}.
 */
public interface ExperienceService {

    /**
     * Save a experience.
     *
     * @param experience the entity to save.
     * @return the persisted entity.
     */
    ResponseEntity<Experience> save(ExperienceDTO experience);
    ResponseEntity<Experience> update(ExperienceDTO experience);

    /**
     * Get all the experiences.
     *
     * @return the list of entities.
     */
    List<Experience> findAll();


    /**
     * Get the "id" experience.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Experience> findOne(Long id);

    /**
     * Delete the "id" experience.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
