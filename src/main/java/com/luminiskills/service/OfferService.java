package com.luminiskills.service;

import com.luminiskills.domain.Offer;
import com.luminiskills.service.dto.AdminOffersDTO;
import com.luminiskills.service.dto.OfferDTO;

import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Offer}.
 */
public interface OfferService {

    /**
     * Save a offer.
     *
     * @param offer the entity to save.
     * @return the persisted entity.
     */
    ResponseEntity<Offer> save(OfferDTO offer);

    ResponseEntity<Offer> update(OfferDTO offer);

    /**
     * Get all the offers.
     *
     * @return the list of entities.
     */
    List<Offer> findAll();

    List<Offer> search(String search);

    /**
     * Get the "id" offer.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Offer> findOne(Long id);

    AdminOffersDTO findAdminOffer(Long id);

    /**
     * Delete the "id" offer.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
