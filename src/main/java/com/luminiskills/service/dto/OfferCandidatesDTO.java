package com.luminiskills.service.dto;

import java.time.LocalDate;
import java.util.List;

import com.luminiskills.domain.Candidate;
import com.luminiskills.domain.Offer;
import com.luminiskills.domain.Recruiter;

public class OfferCandidatesDTO {

    private Long id;
    private LocalDate creationDate;
    private LocalDate lastUpdate;
    private String contractTerm;
    private Integer categoryId;
    private LocalDate expirationDate;
    private Boolean activated;
    private String description;
    private String title;
    private Recruiter recruiter;
    private List<Candidate> candidate;

    /**
     * 
     */
    public OfferCandidatesDTO() {
    }

    public OfferCandidatesDTO(Offer offer, List<Candidate> candidates) {
        this.id = offer.getId();
        this.creationDate = offer.getCreationDate();
        this.lastUpdate = offer.getLastUpdate();
        this.contractTerm = offer.getContractTerm();
        this.categoryId = offer.getCategoryId();
        this.expirationDate = offer.getExpirationDate();
        this.activated = offer.getActivated();
        this.description = offer.getDescription();
        this.title = offer.getTitle();
        this.recruiter = offer.getRecruiter();
        this.candidate = candidates;
    }

    /**
     * @param id
     * @param creationDate
     * @param lastUpdate
     * @param contractTerm
     * @param categoryId
     * @param expirationDate
     * @param activated
     * @param description
     * @param title
     * @param recruiter
     * @param candidate
     */
    public OfferCandidatesDTO(Long id, LocalDate creationDate, LocalDate lastUpdate, String contractTerm,
            Integer categoryId, LocalDate expirationDate, Boolean activated, String description, String title,
            Recruiter recruiter, List<Candidate> candidate) {
        this.id = id;
        this.creationDate = creationDate;
        this.lastUpdate = lastUpdate;
        this.contractTerm = contractTerm;
        this.categoryId = categoryId;
        this.expirationDate = expirationDate;
        this.activated = activated;
        this.description = description;
        this.title = title;
        this.recruiter = recruiter;
        this.candidate = candidate;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the creationDate
     */
    public LocalDate getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the lastUpdate
     */
    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    /**
     * @param lastUpdate the lastUpdate to set
     */
    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    /**
     * @return the contractTerm
     */
    public String getContractTerm() {
        return contractTerm;
    }

    /**
     * @param contractTerm the contractTerm to set
     */
    public void setContractTerm(String contractTerm) {
        this.contractTerm = contractTerm;
    }

    /**
     * @return the categoryId
     */
    public Integer getCategoryId() {
        return categoryId;
    }

    /**
     * @param categoryId the categoryId to set
     */
    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * @return the expirationDate
     */
    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    /**
     * @param expirationDate the expirationDate to set
     */
    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    /**
     * @return the activated
     */
    public Boolean getActivated() {
        return activated;
    }

    /**
     * @param activated the activated to set
     */
    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the recruiter
     */
    public Recruiter getRecruiter() {
        return recruiter;
    }

    /**
     * @param recruiter the recruiter to set
     */
    public void setRecruiter(Recruiter recruiter) {
        this.recruiter = recruiter;
    }

    /**
     * @return the candidate
     */
    public List<Candidate> getCandidate() {
        return candidate;
    }

    /**
     * @param candidate the candidate to set
     */
    public void setCandidate(List<Candidate> candidate) {
        this.candidate = candidate;
    }
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((activated == null) ? 0 : activated.hashCode());
        result = prime * result + ((candidate == null) ? 0 : candidate.hashCode());
        result = prime * result + ((categoryId == null) ? 0 : categoryId.hashCode());
        result = prime * result + ((contractTerm == null) ? 0 : contractTerm.hashCode());
        result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((expirationDate == null) ? 0 : expirationDate.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((lastUpdate == null) ? 0 : lastUpdate.hashCode());
        result = prime * result + ((recruiter == null) ? 0 : recruiter.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        OfferCandidatesDTO other = (OfferCandidatesDTO) obj;
        if (activated == null) {
            if (other.activated != null)
                return false;
        } else if (!activated.equals(other.activated))
            return false;
        if (candidate == null) {
            if (other.candidate != null)
                return false;
        } else if (!candidate.equals(other.candidate))
            return false;
        if (categoryId == null) {
            if (other.categoryId != null)
                return false;
        } else if (!categoryId.equals(other.categoryId))
            return false;
        if (contractTerm == null) {
            if (other.contractTerm != null)
                return false;
        } else if (!contractTerm.equals(other.contractTerm))
            return false;
        if (creationDate == null) {
            if (other.creationDate != null)
                return false;
        } else if (!creationDate.equals(other.creationDate))
            return false;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (expirationDate == null) {
            if (other.expirationDate != null)
                return false;
        } else if (!expirationDate.equals(other.expirationDate))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (lastUpdate == null) {
            if (other.lastUpdate != null)
                return false;
        } else if (!lastUpdate.equals(other.lastUpdate))
            return false;
        if (recruiter == null) {
            if (other.recruiter != null)
                return false;
        } else if (!recruiter.equals(other.recruiter))
            return false;
        if (title == null) {
            if (other.title != null)
                return false;
        } else if (!title.equals(other.title))
            return false;
        return true;
    }
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */

    @Override
    public String toString() {
        return "OfferCandidatesDTO [activated=" + activated + ", candidate=" + candidate + ", categoryId=" + categoryId
                + ", contractTerm=" + contractTerm + ", creationDate=" + creationDate + ", description=" + description
                + ", expirationDate=" + expirationDate + ", id=" + id + ", lastUpdate=" + lastUpdate + ", recruiter="
                + recruiter + ", title=" + title + "]";
    }

}
