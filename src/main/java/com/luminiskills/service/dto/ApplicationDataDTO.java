package com.luminiskills.service.dto;

public class ApplicationDataDTO {
    private Long id;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @param id
     */
    public ApplicationDataDTO(Long id) {
        this.id = id;
    }

    /**
     * 
     */
    public ApplicationDataDTO() {
    }
}
