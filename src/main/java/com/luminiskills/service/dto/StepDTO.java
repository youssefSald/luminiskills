package com.luminiskills.service.dto;

import java.time.LocalDate;

public class StepDTO {

    private Long id;
    private String name;
    private LocalDate creationDate;
    private LocalDate closeDate;
    private Long application;

    public StepDTO() {
    }

    /**
     * @param id
     * @param name
     * @param creationDate
     * @param closeDate
     * @param application
     */
    public StepDTO(Long id, String name, LocalDate creationDate, LocalDate closeDate, Long application) {
        this.id = id;
        this.name = name;
        this.creationDate = creationDate;
        this.closeDate = closeDate;
        this.application = application;
    }

    public StepDTO(String name, LocalDate creationDate, LocalDate closeDate, Long application) {
        this.name = name;
        this.creationDate = creationDate;
        this.closeDate = closeDate;
        this.application = application;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(LocalDate closeDate) {
        this.closeDate = closeDate;
    }

    public Long getApplication() {
        return application;
    }

    public void setApplication(Long application) {
        this.application = application;
    }

}
