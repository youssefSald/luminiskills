package com.luminiskills.service.dto;

public class SearchDTO {

    private String search;

    /**
     * 
     */
    public SearchDTO() {
    }

    /**
     * @param search
     */
    public SearchDTO(String search) {
        this.search = search;
    }

    /**
     * @return the search
     */
    public String getSearch() {
        return search;
    }

    /**
     * @param search the search to set
     */
    public void setSearch(String search) {
        this.search = search;
    }

}
