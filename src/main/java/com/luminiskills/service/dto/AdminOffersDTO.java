package com.luminiskills.service.dto;

import java.util.List;

import com.luminiskills.domain.Offer;
import com.luminiskills.domain.User;

public class AdminOffersDTO {
    
    private Offer offer;
    private List<User> users;
    /**
     * 
     */
    public AdminOffersDTO() {
    }
    /**
     * @param offer
     * @param users
     */
    public AdminOffersDTO(Offer offer, List<User> users) {
        this.offer = offer;
        this.users = users;
    }
    /**
     * @return the offer
     */
    public Offer getOffer() {
        return offer;
    }
    /**
     * @param offer the offer to set
     */
    public void setOffer(Offer offer) {
        this.offer = offer;
    }
    /**
     * @return the users
     */
    public List<User> getUsers() {
        return users;
    }
    /**
     * @param users the users to set
     */
    public void setUsers(List<User> users) {
        this.users = users;
    }



}
