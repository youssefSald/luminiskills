package com.luminiskills.service.dto;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Size;

public class UserImageDTO {
    private Long id;

    private MultipartFile multipartFileimage;

    public Long getId() {
        return id;
    }
    public String url;
    public void setId(Long id) {
        this.id = id;
    }

    public MultipartFile getMultipartFileimage() {
        return multipartFileimage;
    }

    public void setMultipartFileimage(MultipartFile multipartFileimage) {
        this.multipartFileimage = multipartFileimage;
    }

    public UserImageDTO() {
    }

    public UserImageDTO(Long id, MultipartFile multipartFileimage, String url) {
        this.id = id;
        this.multipartFileimage = multipartFileimage;
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public UserImageDTO(Long id, @Size(min = 1, max = 50) MultipartFile multipartFileimage) {
        this.id = id;
        this.multipartFileimage = multipartFileimage;
    }
}
