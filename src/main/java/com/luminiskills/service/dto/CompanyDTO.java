package com.luminiskills.service.dto;

import java.util.*;

public class CompanyDTO {

    private Long id;
    private String logo;
    private String name;
    private String address;
    private Integer postalCode;
    private String city;
    private String country;
    private Set<Long> recruiters = new HashSet<>();
    
    public CompanyDTO() {
    }

    /**
     * @param id
     * @param logo
     * @param name
     * @param address
     * @param postalCode
     * @param city
     * @param country
     * @param recruiters
     */
    public CompanyDTO(Long id, String logo, String name, String address, Integer postalCode, String city,
            String country, Set<Long> recruiters) {
        this.id = id;
        this.logo = logo;
        this.name = name;
        this.address = address;
        this.postalCode = postalCode;
        this.city = city;
        this.country = country;
        this.recruiters = recruiters;
    }

    public CompanyDTO(String logo, String name, String address, Integer postalCode, String city, String country,
            Set<Long> recruiters) {
        this.logo = logo;
        this.name = name;
        this.address = address;
        this.postalCode = postalCode;
        this.city = city;
        this.country = country;
        this.recruiters = recruiters;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Set<Long> getRecruiters() {
        return recruiters;
    }

    public void setRecruiters(Set<Long> recruiters) {
        this.recruiters = recruiters;
    }


}
