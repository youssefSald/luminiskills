package com.luminiskills.service.dto;

public class AppliedDTO {
    private Long candidateId;
    private Long offerId;

    /**
     * 
     */
    public AppliedDTO() {
    }
    /**
     * @param candidateId
     * @param offerId
     */
    public AppliedDTO(Long candidateId, Long offerId) {
        this.candidateId = candidateId;
        this.offerId = offerId;
    }

    /**
     * @return the candidateId
     */
    public Long getCandidateId() {
        return candidateId;
    }
    /**
     * @param candidateId the candidateId to set
     */
    public void setCandidateId(Long candidateId) {
        this.candidateId = candidateId;
    }
    /**
     * @return the offerId
     */
    public Long getOfferId() {
        return offerId;
    }
    /**
     * @param offerId the offerId to set
     */
    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }
    
    
}
