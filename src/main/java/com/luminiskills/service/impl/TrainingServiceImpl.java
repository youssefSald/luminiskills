package com.luminiskills.service.impl;

import com.luminiskills.service.CandidateService;
import com.luminiskills.service.TrainingService;
import com.luminiskills.service.dto.TrainingDTO;
import com.luminiskills.domain.Candidate;
import com.luminiskills.domain.Training;
import com.luminiskills.repository.TrainingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Training}.
 */
@Service
@Transactional
public class TrainingServiceImpl implements TrainingService {

    private final Logger log = LoggerFactory.getLogger(TrainingServiceImpl.class);

    private final TrainingRepository trainingRepository;

    @Autowired
    CandidateService candidateService;

    public TrainingServiceImpl(TrainingRepository trainingRepository) {
        this.trainingRepository = trainingRepository;
    }

    @Override
    public ResponseEntity<Training> save(TrainingDTO training) {
        log.debug("Request to save Training : {}", training);
        if (training.getCandidate() == null) {
            return new ResponseEntity<Training>(HttpStatus.BAD_REQUEST);
        }
        Candidate candidate = candidateService.findOne(training.getCandidate()).get();
        Training train = new Training(training.getDiploma(), training.getUniversity(), training.getFromDate(),
                training.getToDate(), training.getCity(), candidate);
        return new ResponseEntity<Training>(trainingRepository.save(train), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Training> update(TrainingDTO training) {
        log.debug("Request to save Training : {}", training);
        Candidate candidate = candidateService.findOne(training.getCandidate()).get();
        Training train = new Training(training.getId(), training.getDiploma(), training.getUniversity(),
                training.getFromDate(), training.getToDate(), training.getCity(), candidate);
        return new ResponseEntity<Training>(trainingRepository.save(train), HttpStatus.CREATED);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Training> findAll() {
        log.debug("Request to get all Trainings");
        return trainingRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Training> findOne(Long id) {
        log.debug("Request to get Training : {}", id);
        return trainingRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Training : {}", id);
        trainingRepository.deleteById(id);
    }
}
