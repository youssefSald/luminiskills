package com.luminiskills.service.impl;

import java.util.List;
import java.util.Optional;

import com.luminiskills.domain.Message;
import com.luminiskills.repository.MessageRepository;
import com.luminiskills.service.MessageService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MessageServiceImpl implements MessageService {

    private final Logger log = LoggerFactory.getLogger(MessageServiceImpl.class);

    @Autowired
    MessageRepository messageRepository;

    public MessageServiceImpl() {
    }

    @Override
    public ResponseEntity<Message> save(Message message) {
        log.debug("Request to save Experience : {}", message);

        return new ResponseEntity<Message>(messageRepository.save(message), HttpStatus.CREATED);

    }

    @Override
    @Transactional(readOnly = true)
    public List<Message> findAll() {
        log.debug("Request to get all Experiences");
        return messageRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Message> findOne(Long id) {
        log.debug("Request to get Experience : {}", id);
        return messageRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Experience : {}", id);
        messageRepository.deleteById(id);
    }
}
