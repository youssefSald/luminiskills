package com.luminiskills.service.impl;

import com.luminiskills.service.ExperienceService;
import com.luminiskills.service.dto.ExperienceDTO;
import com.luminiskills.domain.Candidate;
import com.luminiskills.domain.Experience;
import com.luminiskills.repository.ExperienceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import liquibase.pro.packaged.ex;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Experience}.
 */
@Service
@Transactional
public class ExperienceServiceImpl implements ExperienceService {

    private final Logger log = LoggerFactory.getLogger(ExperienceServiceImpl.class);

    private final ExperienceRepository experienceRepository;

    @Autowired
    CandidateServiceImpl candidateServiceImpl;

    public ExperienceServiceImpl(ExperienceRepository experienceRepository) {
        this.experienceRepository = experienceRepository;
    }

    @Override
    public ResponseEntity<Experience> save(ExperienceDTO experience) {
        log.debug("Request to save Experience : {}", experience);
        if (experience.getCandidate() == null) {
            return new ResponseEntity<Experience>(HttpStatus.BAD_REQUEST);
        }
        Candidate candidate = candidateServiceImpl.findOne(experience.getCandidate()).get();
        Experience exp = new Experience(experience.getTitle(), experience.getInstitution(), experience.getContract(),
                experience.getFromDate(), experience.getToDate(), experience.getLocation(), experience.getSkills(),
                experience.getLevel(), candidate);
        return new ResponseEntity<Experience>(experienceRepository.save(exp), HttpStatus.CREATED);

    }

    @Override
    public ResponseEntity<Experience> update(ExperienceDTO experience) {
        log.debug("Request to save Experience : {}", experience);
        if (experience.getCandidate() == null) {
            return new ResponseEntity<Experience>(HttpStatus.BAD_REQUEST);
        }
        Candidate candidate = candidateServiceImpl.findOne(experience.getCandidate()).get();
        Experience exp = new Experience(experience.getId(), experience.getTitle(), experience.getInstitution(),
                experience.getContract(), experience.getFromDate(), experience.getToDate(), experience.getLocation(),
                experience.getSkills(), experience.getLevel(), candidate);
        return new ResponseEntity<Experience>(experienceRepository.save(exp), HttpStatus.CREATED);

    }

    @Override
    @Transactional(readOnly = true)
    public List<Experience> findAll() {
        log.debug("Request to get all Experiences");
        return experienceRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Experience> findOne(Long id) {
        log.debug("Request to get Experience : {}", id);
        return experienceRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Experience : {}", id);
        experienceRepository.deleteById(id);
    }
}
