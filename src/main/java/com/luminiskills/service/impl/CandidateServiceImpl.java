package com.luminiskills.service.impl;

import com.luminiskills.service.ApplicationService;
import com.luminiskills.service.CandidateService;
import com.luminiskills.service.ExperienceService;
import com.luminiskills.service.TrainingService;
import com.luminiskills.service.UserService;
import com.luminiskills.service.dto.CandidateDTO;
import com.luminiskills.domain.Application;
import com.luminiskills.domain.Candidate;
import com.luminiskills.domain.Experience;
import com.luminiskills.domain.Training;
import com.luminiskills.domain.User;
import com.luminiskills.repository.CandidateRepository;
import com.luminiskills.repository.UserRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Service Implementation for managing {@link Candidate}.
 */
@Service
@Transactional
public class CandidateServiceImpl implements CandidateService {

    private final Logger log = LoggerFactory.getLogger(CandidateServiceImpl.class);

    private final CandidateRepository candidateRepository;

    @Autowired
    ApplicationService applicationService;

    @Autowired
    ExperienceService experienceService;

    @Autowired
    TrainingService trainingService;

    @Autowired
    UserRepository userRepository;

    public CandidateServiceImpl(CandidateRepository candidateRepository) {
        this.candidateRepository = candidateRepository;
    }

    @Override
    public ResponseEntity<Candidate> save(CandidateDTO candidate) {
        log.debug("Request to save Candidate : {}", candidate);

        if (candidate.getUser() == null) {
            return new ResponseEntity<Candidate>(HttpStatus.BAD_REQUEST);
        }
        User user = userRepository.findById(candidate.getUser()).get();
        Candidate cnd = new Candidate(candidate.getDriverslicense(), candidate.getMobility(), candidate.getCity(),
                candidate.getAddress(), candidate.getCv(), candidate.getBithDate(), candidate.getHandicapSituation(),
                candidate.getFullProfil(), candidate.getWithoutExperience(), user);
        return new ResponseEntity<Candidate>(candidateRepository.save(cnd), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Candidate> update(CandidateDTO candidate) {
        log.debug("Request to save Candidate : {}", candidate);

        if (candidate.getUser() == null) {
            return new ResponseEntity<Candidate>(HttpStatus.BAD_REQUEST);
        }
        User user = userRepository.findById(candidate.getUser()).get();
        Candidate cnd = new Candidate(candidate.getId(), candidate.getDriverslicense(), candidate.getMobility(),
                candidate.getCity(), candidate.getAddress(), candidate.getCv(), candidate.getBithDate(),
                candidate.getHandicapSituation(), candidate.getFullProfil(), candidate.getWithoutExperience(), user);
        return new ResponseEntity<Candidate>(candidateRepository.save(cnd), HttpStatus.CREATED);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Candidate> findAll() {
        log.debug("Request to get all Candidates");
        return candidateRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Candidate> findOne(Long id) {
        log.debug("Request to get Candidate : {}", id);
        return candidateRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Candidate> findcurrentOne(Long id) {
        log.debug("Request to get Candidate : {}", id);
        return candidateRepository.findCurrentById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Candidate : {}", id);
        candidateRepository.deleteById(id);
    }
}
