package com.luminiskills.service.impl;

import com.luminiskills.service.ApplicationService;
import com.luminiskills.service.dto.ApplicationDTO;
import com.luminiskills.service.dto.ApplicationDataDTO;
import com.luminiskills.service.dto.AppliedDTO;
import com.luminiskills.domain.Application;
import com.luminiskills.domain.Candidate;
import com.luminiskills.domain.Offer;
import com.luminiskills.domain.Step;
import com.luminiskills.domain.User;
import com.luminiskills.repository.ApplicationRepository;
import com.luminiskills.repository.UserRepository;
import com.luminiskills.security.SecurityUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Application}.
 */
@Service
@Transactional
public class ApplicationServiceImpl implements ApplicationService {

    private final Logger log = LoggerFactory.getLogger(ApplicationServiceImpl.class);

    private final ApplicationRepository applicationRepository;

    @Autowired
    CandidateServiceImpl candidateServiceImpl;

    @Autowired
    OfferServiceImpl offerServiceImpl;

    @Autowired
    UserRepository userRepository;

    @Autowired
    StepServiceImpl stepServiceImpl;

    public ApplicationServiceImpl(ApplicationRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }

    @Override
    public ResponseEntity<Application> save(ApplicationDTO application) {
        log.debug("Request to save Application : {}", application);
        if (application.getCandidate() == null || application.getOffer() == null || application.getStep() == null) {
            return new ResponseEntity<Application>(HttpStatus.BAD_REQUEST);
        }
        Candidate candidate = candidateServiceImpl.findOne(application.getCandidate()).get();
        Offer offer = offerServiceImpl.findOne(application.getOffer()).get();
        Step step = stepServiceImpl.findOne(application.getStep()).get();
        Application app = new Application(application.getCreationDate(), application.getNewApplication(),
                application.getState(), application.getComment(), step, offer, candidate);
        return new ResponseEntity<Application>(applicationRepository.save(app), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Application> update(ApplicationDTO application) {
        log.debug("Request to save Application : {}", application);
        if (application.getCandidate() == null || application.getOffer() == null || application.getStep() == null) {
            return new ResponseEntity<Application>(HttpStatus.BAD_REQUEST);
        }
        Candidate candidate = candidateServiceImpl.findOne(application.getCandidate()).get();
        Offer offer = offerServiceImpl.findOne(application.getOffer()).get();
        Step step = stepServiceImpl.findOne(application.getStep()).get();
        Application app = new Application(application.getId(), application.getCreationDate(),
                application.getNewApplication(), application.getState(), application.getComment(), step, offer,
                candidate);
        return new ResponseEntity<Application>(applicationRepository.save(app), HttpStatus.CREATED);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Application> findAll() {
        log.debug("Request to get all Applications");
        return applicationRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Application findApplication(ApplicationDataDTO id) {
        log.debug("Request to get all Applications");
        Application application = applicationRepository.findById(id.getId()).get();
        Offer offer = application.getOffer();
        Candidate candidate = application.getCandidate();
        User user = candidate.getUser();
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public Application applied(Long applied) {
        Optional<User> user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin().get());
        Candidate candidate = candidateServiceImpl.findcurrentOne(user.get().getId()).get();
        return applicationRepository.applied(candidate.getId(), applied);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Application> findOne(Long id) {
        log.debug("Request to get Application : {}", id);
        return applicationRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Application : {}", id);
        applicationRepository.deleteById(id);
    }
}
