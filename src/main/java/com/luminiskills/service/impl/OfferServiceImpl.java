package com.luminiskills.service.impl;

import com.luminiskills.service.OfferService;
import com.luminiskills.service.dto.AdminOffersDTO;
import com.luminiskills.service.dto.ApplicationDataDTO;
import com.luminiskills.service.dto.OfferDTO;
import com.luminiskills.domain.Application;
import com.luminiskills.domain.Candidate;
import com.luminiskills.domain.Offer;
import com.luminiskills.domain.Recruiter;
import com.luminiskills.domain.User;
import com.luminiskills.repository.ApplicationRepository;
import com.luminiskills.repository.CandidateRepository;
import com.luminiskills.repository.OfferRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Service Implementation for managing {@link Offer}.
 */
@Service
@Transactional
public class OfferServiceImpl implements OfferService {

    private final Logger log = LoggerFactory.getLogger(OfferServiceImpl.class);

    @Autowired
    RecruiterServiceImpl recruiterServiceImpl;

    private final OfferRepository offerRepository;

    @Autowired
    ApplicationRepository applicationRepository;

    @Autowired
    CandidateRepository candidateRepository;

    public OfferServiceImpl(OfferRepository offerRepository) {
        this.offerRepository = offerRepository;
    }

    @Override
    public ResponseEntity<Offer> save(OfferDTO offer) {
        log.debug("Request to save Offer : {}", offer);
        if (offer.getRecruiter() == null) {
            return new ResponseEntity<Offer>(HttpStatus.BAD_REQUEST);
        }
        Optional<Recruiter> recruiter = recruiterServiceImpl.findOne(offer.getRecruiter());
        Offer of = new Offer(offer.getCreationDate(), offer.getLastUpdate(), offer.getContractTerm(),
                offer.getCategoryId(), offer.getExpirationDate(), offer.getActivated(), offer.getDescription(),
                offer.getTitle(), recruiter.get());

        return new ResponseEntity<Offer>(offerRepository.save(of), HttpStatus.CREATED);

    }

    @Override
    public ResponseEntity<Offer> update(OfferDTO offer) {
        log.debug("Request to save Offer : {}", offer);
        if (offer.getRecruiter() == null) {
            return new ResponseEntity<Offer>(HttpStatus.BAD_REQUEST);
        }
        Optional<Recruiter> recruiter = recruiterServiceImpl.findOne(offer.getRecruiter());
        Offer of = new Offer(offer.getId(), offer.getCreationDate(), offer.getLastUpdate(), offer.getContractTerm(),
                offer.getCategoryId(), offer.getExpirationDate(), offer.getActivated(), offer.getDescription(),
                offer.getTitle(), recruiter.get());

        return new ResponseEntity<Offer>(offerRepository.save(of), HttpStatus.CREATED);

    }

    @Override
    @Transactional(readOnly = true)
    public List<Offer> findAll() {
        log.debug("Request to get all Offers");
        return offerRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Offer> search(String search) {
        log.debug("Request to get all Offers");
        return offerRepository.search(search);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Offer> findOne(Long id) {
        log.debug("Request to get Offer : {}", id);
        return offerRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public AdminOffersDTO findAdminOffer(Long offerId) {
        log.debug("Request to get OfferApplications : {}", offerId);
        List<Application> applications = applicationRepository.findApplications(offerId);
        List<Candidate> candidates = new ArrayList<>();
        List<User> users = new ArrayList<>();
        for (Application application : applications) {
            candidates.add(application.getCandidate());
        }
        for (Candidate candidate : candidates) {
            users.add(candidate.getUser());
        }
        Offer of = offerRepository.findById(offerId).get();
        AdminOffersDTO adminoffers = new AdminOffersDTO();
        adminoffers.setOffer(of);
        adminoffers.setUsers(users);
        // List<Application> applications = offerRepository.findAllById(ids);
        return adminoffers;
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Offer : {}", id);
        offerRepository.deleteById(id);
    }
}
