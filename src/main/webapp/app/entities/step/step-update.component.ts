import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IStep, Step } from 'app/shared/model/step.model';
import { StepService } from './step.service';
import { IApplication } from 'app/shared/model/application.model';
import { ApplicationService } from 'app/entities/application/application.service';

@Component({
  selector: 'jhi-step-update',
  templateUrl: './step-update.component.html',
})
export class StepUpdateComponent implements OnInit {
  isSaving = false;
  applicationids: IApplication[] = [];
  creationDateDp: any;
  closeDateDp: any;

  editForm = this.fb.group({
    id: [],
    name: [],
    creationDate: [],
    closeDate: [],
    applicationId: [],
  });

  constructor(
    protected stepService: StepService,
    protected applicationService: ApplicationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ step }) => {
      this.updateForm(step);

      this.applicationService
        .query({ filter: 'step-is-null' })
        .pipe(
          map((res: HttpResponse<IApplication[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IApplication[]) => {
          if (!step.applicationId || !step.applicationId.id) {
            this.applicationids = resBody;
          } else {
            this.applicationService
              .find(step.applicationId.id)
              .pipe(
                map((subRes: HttpResponse<IApplication>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IApplication[]) => (this.applicationids = concatRes));
          }
        });
    });
  }

  updateForm(step: IStep): void {
    this.editForm.patchValue({
      id: step.id,
      name: step.name,
      creationDate: step.creationDate,
      closeDate: step.closeDate,
      applicationId: step.applicationId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const step = this.createFromForm();
    if (step.id !== undefined) {
      this.subscribeToSaveResponse(this.stepService.update(step));
    } else {
      this.subscribeToSaveResponse(this.stepService.create(step));
    }
  }

  private createFromForm(): IStep {
    return {
      ...new Step(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      creationDate: this.editForm.get(['creationDate'])!.value,
      closeDate: this.editForm.get(['closeDate'])!.value,
      applicationId: this.editForm.get(['applicationId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStep>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IApplication): any {
    return item.id;
  }
}
