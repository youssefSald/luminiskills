import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LuminiskillsSharedModule } from 'app/shared/shared.module';
import { RecruiterComponent } from './recruiter.component';
import { RecruiterDetailComponent } from './recruiter-detail.component';
import { RecruiterUpdateComponent } from './recruiter-update.component';
import { RecruiterDeleteDialogComponent } from './recruiter-delete-dialog.component';
import { recruiterRoute } from './recruiter.route';

@NgModule({
  imports: [LuminiskillsSharedModule, RouterModule.forChild(recruiterRoute)],
  declarations: [RecruiterComponent, RecruiterDetailComponent, RecruiterUpdateComponent, RecruiterDeleteDialogComponent],
  entryComponents: [RecruiterDeleteDialogComponent],
})
export class LuminiskillsRecruiterModule {}
