import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRecruiter } from 'app/shared/model/recruiter.model';

@Component({
  selector: 'jhi-recruiter-detail',
  templateUrl: './recruiter-detail.component.html',
})
export class RecruiterDetailComponent implements OnInit {
  recruiter: IRecruiter | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ recruiter }) => (this.recruiter = recruiter));
  }

  previousState(): void {
    window.history.back();
  }
}
