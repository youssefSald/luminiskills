import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRecruiter } from 'app/shared/model/recruiter.model';
import { RecruiterService } from './recruiter.service';

@Component({
  templateUrl: './recruiter-delete-dialog.component.html',
})
export class RecruiterDeleteDialogComponent {
  recruiter?: IRecruiter;

  constructor(protected recruiterService: RecruiterService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.recruiterService.delete(id).subscribe(() => {
      this.eventManager.broadcast('recruiterListModification');
      this.activeModal.close();
    });
  }
}
