import { IOffer } from 'app/shared/model/offer.model';
import { ICompany } from 'app/shared/model/company.model';

export interface IRecruiter {
  id?: number;
  post?: string;
  offers?: IOffer[];
  companyId?: ICompany;
}

export class Recruiter implements IRecruiter {
  constructor(public id?: number, public post?: string, public offers?: IOffer[], public companyId?: ICompany) {}
}
