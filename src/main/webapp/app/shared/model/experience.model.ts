import { Moment } from 'moment';
import { ICandidate } from 'app/shared/model/candidate.model';

export interface IExperience {
  id?: number;
  title?: string;
  institution?: string;
  contract?: string;
  fromDate?: Moment;
  toDate?: Moment;
  location?: string;
  skills?: string;
  level?: string;
  candidateId?: ICandidate;
}

export class Experience implements IExperience {
  constructor(
    public id?: number,
    public title?: string,
    public institution?: string,
    public contract?: string,
    public fromDate?: Moment,
    public toDate?: Moment,
    public location?: string,
    public skills?: string,
    public level?: string,
    public candidateId?: ICandidate
  ) {}
}
