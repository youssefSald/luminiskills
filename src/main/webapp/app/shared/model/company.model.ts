import { IRecruiter } from 'app/shared/model/recruiter.model';

export interface ICompany {
  id?: number;
  logo?: string;
  name?: string;
  address?: string;
  postalCode?: number;
  city?: string;
  country?: string;
  recruiters?: IRecruiter[];
}

export class Company implements ICompany {
  constructor(
    public id?: number,
    public logo?: string,
    public name?: string,
    public address?: string,
    public postalCode?: number,
    public city?: string,
    public country?: string,
    public recruiters?: IRecruiter[]
  ) {}
}
