import { Moment } from 'moment';
import { IApplication } from 'app/shared/model/application.model';

export interface IStep {
  id?: number;
  name?: string;
  creationDate?: Moment;
  closeDate?: Moment;
  applicationId?: IApplication;
}

export class Step implements IStep {
  constructor(
    public id?: number,
    public name?: string,
    public creationDate?: Moment,
    public closeDate?: Moment,
    public applicationId?: IApplication
  ) {}
}
