import { Moment } from 'moment';
import { IStep } from 'app/shared/model/step.model';
import { IOffer } from 'app/shared/model/offer.model';
import { ICandidate } from 'app/shared/model/candidate.model';

export interface IApplication {
  id?: number;
  creationDate?: Moment;
  newApplication?: boolean;
  state?: string;
  comment?: string;
  stepId?: IStep;
  offers?: IOffer[];
  candidateId?: ICandidate;
}

export class Application implements IApplication {
  constructor(
    public id?: number,
    public creationDate?: Moment,
    public newApplication?: boolean,
    public state?: string,
    public comment?: string,
    public stepId?: IStep,
    public offers?: IOffer[],
    public candidateId?: ICandidate
  ) {
    this.newApplication = this.newApplication || false;
  }
}
