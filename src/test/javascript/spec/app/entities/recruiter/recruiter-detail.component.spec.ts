import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { LuminiskillsTestModule } from '../../../test.module';
import { RecruiterDetailComponent } from 'app/entities/recruiter/recruiter-detail.component';
import { Recruiter } from 'app/shared/model/recruiter.model';

describe('Component Tests', () => {
  describe('Recruiter Management Detail Component', () => {
    let comp: RecruiterDetailComponent;
    let fixture: ComponentFixture<RecruiterDetailComponent>;
    const route = ({ data: of({ recruiter: new Recruiter(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [LuminiskillsTestModule],
        declarations: [RecruiterDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(RecruiterDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RecruiterDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load recruiter on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.recruiter).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
