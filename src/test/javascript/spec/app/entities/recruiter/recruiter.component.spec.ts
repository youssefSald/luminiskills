import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { LuminiskillsTestModule } from '../../../test.module';
import { RecruiterComponent } from 'app/entities/recruiter/recruiter.component';
import { RecruiterService } from 'app/entities/recruiter/recruiter.service';
import { Recruiter } from 'app/shared/model/recruiter.model';

describe('Component Tests', () => {
  describe('Recruiter Management Component', () => {
    let comp: RecruiterComponent;
    let fixture: ComponentFixture<RecruiterComponent>;
    let service: RecruiterService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [LuminiskillsTestModule],
        declarations: [RecruiterComponent],
      })
        .overrideTemplate(RecruiterComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RecruiterComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RecruiterService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Recruiter(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.recruiters && comp.recruiters[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
