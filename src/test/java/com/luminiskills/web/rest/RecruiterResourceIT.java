package com.luminiskills.web.rest;

import com.luminiskills.LuminiskillsApp;
import com.luminiskills.domain.Recruiter;
import com.luminiskills.repository.RecruiterRepository;
import com.luminiskills.service.RecruiterService;
import com.luminiskills.service.dto.RecruiterDTO;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RecruiterResource} REST controller.
 */
@SpringBootTest(classes = LuminiskillsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class RecruiterResourceIT {

    private static final String DEFAULT_POST = "AAAAAAAAAA";
    private static final String UPDATED_POST = "BBBBBBBBBB";

    @Autowired
    private RecruiterRepository recruiterRepository;

    @Autowired
    private RecruiterService recruiterService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRecruiterMockMvc;

    private Recruiter recruiter;
    private RecruiterDTO rc;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static Recruiter createEntity(EntityManager em) {
        Recruiter recruiter = new Recruiter().post(DEFAULT_POST);
        return recruiter;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static Recruiter createUpdatedEntity(EntityManager em) {
        Recruiter recruiter = new Recruiter().post(UPDATED_POST);
        return recruiter;
    }

    @BeforeEach
    public void initTest() {
        recruiter = createEntity(em);
    }

    @Test
    @Transactional
    public void createRecruiter() throws Exception {
        int databaseSizeBeforeCreate = recruiterRepository.findAll().size();
        // Create the Recruiter
        restRecruiterMockMvc.perform(post("/api/recruiters").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(recruiter))).andExpect(status().isCreated());

        // Validate the Recruiter in the database
        List<Recruiter> recruiterList = recruiterRepository.findAll();
        assertThat(recruiterList).hasSize(databaseSizeBeforeCreate + 1);
        Recruiter testRecruiter = recruiterList.get(recruiterList.size() - 1);
        assertThat(testRecruiter.getPost()).isEqualTo(DEFAULT_POST);
    }

    @Test
    @Transactional
    public void createRecruiterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = recruiterRepository.findAll().size();

        // Create the Recruiter with an existing ID
        recruiter.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRecruiterMockMvc.perform(post("/api/recruiters").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(recruiter))).andExpect(status().isBadRequest());

        // Validate the Recruiter in the database
        List<Recruiter> recruiterList = recruiterRepository.findAll();
        assertThat(recruiterList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRecruiters() throws Exception {
        // Initialize the database
        recruiterRepository.saveAndFlush(recruiter);

        // Get all the recruiterList
        restRecruiterMockMvc.perform(get("/api/recruiters?sort=id,desc")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(recruiter.getId().intValue())))
                .andExpect(jsonPath("$.[*].post").value(hasItem(DEFAULT_POST)));
    }

    @Test
    @Transactional
    public void getRecruiter() throws Exception {
        // Initialize the database
        recruiterRepository.saveAndFlush(recruiter);

        // Get the recruiter
        restRecruiterMockMvc.perform(get("/api/recruiters/{id}", recruiter.getId())).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(recruiter.getId().intValue()))
                .andExpect(jsonPath("$.post").value(DEFAULT_POST));
    }

    @Test
    @Transactional
    public void getNonExistingRecruiter() throws Exception {
        // Get the recruiter
        restRecruiterMockMvc.perform(get("/api/recruiters/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRecruiter() throws Exception {
        // Initialize the database
        recruiterService.save(rc);

        int databaseSizeBeforeUpdate = recruiterRepository.findAll().size();

        // Update the recruiter
        Recruiter updatedRecruiter = recruiterRepository.findById(recruiter.getId()).get();
        // Disconnect from session so that the updates on updatedRecruiter are not
        // directly saved in db
        em.detach(updatedRecruiter);
        updatedRecruiter.post(UPDATED_POST);

        restRecruiterMockMvc.perform(put("/api/recruiters").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(updatedRecruiter))).andExpect(status().isOk());

        // Validate the Recruiter in the database
        List<Recruiter> recruiterList = recruiterRepository.findAll();
        assertThat(recruiterList).hasSize(databaseSizeBeforeUpdate);
        Recruiter testRecruiter = recruiterList.get(recruiterList.size() - 1);
        assertThat(testRecruiter.getPost()).isEqualTo(UPDATED_POST);
    }

    @Test
    @Transactional
    public void updateNonExistingRecruiter() throws Exception {
        int databaseSizeBeforeUpdate = recruiterRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRecruiterMockMvc.perform(put("/api/recruiters").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(recruiter))).andExpect(status().isBadRequest());

        // Validate the Recruiter in the database
        List<Recruiter> recruiterList = recruiterRepository.findAll();
        assertThat(recruiterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRecruiter() throws Exception {
        // Initialize the database
        recruiterService.save(rc);

        int databaseSizeBeforeDelete = recruiterRepository.findAll().size();

        // Delete the recruiter
        restRecruiterMockMvc
                .perform(delete("/api/recruiters/{id}", recruiter.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Recruiter> recruiterList = recruiterRepository.findAll();
        assertThat(recruiterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
